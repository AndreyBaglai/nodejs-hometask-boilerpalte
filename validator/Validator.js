class Validator {
  constructor(key, value) {
      this.key = key;
      this.value = value;
  }

  isRequired() {
      if (!this.value) {
          throw new Error(`${this.key} is required`);
      }
      return this;
  }
  
  isValidNames() {
    const regExp = /[A-Za-z]/;
    const maxLength = 15;
    if (!this.value.match(regExp)) {
      throw new Error(`${this.key} is not valid`);
    } else if (this.value.length > maxLength) {
      throw new Error(`${this.key} must be 15 characters maximum`);
    }
    return this;
  }

  isValidEmail() {
      const regExp = /@gmail.com$/;
      if (!this.value.match(regExp)) {
          throw new Error(`${this.key} is not valid`);
      }
      return this;
  }

  isPhoneValid() {
    const regExp = /^\+\d{12}/;
    if (!phone.match(regExp)) {
        throw new Error(`${this.key} is not valid`);
    }
    return this;
  }

  isPasswordValid() {
    const minSize = 4;
    if (this.value.length < minSize) {
      throw new Error(`${this.key} must be minimum 4`);
    }
    return this;
  }
}

module.exports = Validator;



