const { user } = require('../models/user');
const Validator = require('../validator/Validator');

const validateUser = (req, res, next) => {
    const firstNameValidator = new Validator('firstName', req.body.firstName);
    const lastNameValidator = new Validator('lastName', req.body.lastName);
    const emailValidator = new Validator('email', req.body.email);
    const phoneNumberValidator = new Validator('phoneNumber', req.body.phoneNumber);
    const passwordValidator = new Validator('password', req.body.password);

    try {
        firstNameValidator.isRequired().isValidNames();
        lastNameValidator.isRequired().isValidNames();
        emailValidator.isRequired().isValidEmail();
        phoneNumberValidator.isRequired();
        passwordValidator.isRequired().isPasswordValid();
        next();
    } catch (err) {
        next(
            {
                message: err.message,
                status: 400,
            },
            req,
            res,
            next
        );
    }
};

exports.validateUser = validateUser;

