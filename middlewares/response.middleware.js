const responseMiddleware = (req, res, next) => {
    next(res.status(200).send({data: res.body}));
};

const responseMiddlewareCreateUser = (req, res, next) => {
    next(res.status(201).send({data: res.body}));
};

const responseMiddlewareByIdUser = (req, res, next) => {
    if (!res.body) next(res.status(404).send('User not found'));
    next(res.status(200).send({data: res.body}));
};

const responseMiddlewareDeleteUser = (req, res, next) => {
    if (!res.body) next(res.status(400).send('Can\'t delete user'));
    next(res.status(200).send({data: res.body}));
};

const responseMiddlewareAllUsers = (req, res, next) => {
    next(res.status(200).send({data: res.body}));
};

const responseMiddlewareUpdateUser = (req, res, next) => {
    next(res.status(201).send({data: res.body}));
};

exports.responseMiddleware = responseMiddleware;
exports.responseMiddlewareCreateUser = responseMiddlewareCreateUser;
exports.responseMiddlewareAllUsers = responseMiddlewareAllUsers;
exports.responseMiddlewareByIdUser = responseMiddlewareByIdUser;
exports.responseMiddlewareDeleteUser = responseMiddlewareDeleteUser;
exports.responseMiddlewareUpdateUser = responseMiddlewareUpdateUser;
