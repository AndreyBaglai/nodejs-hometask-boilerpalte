const { Router } = require('express');
const { validateUser } = require('../middlewares/user.validation.middleware');
const { responseMiddlewareCreateUser, responseMiddlewareAllUsers, responseMiddlewareByIdUser, responseMiddlewareDeleteUser, responseMiddlewareUpdateUser } = require('../middlewares/response.middleware');
const UserService = require('../services/userService');

const router = Router();

const createUser = (req, res, next) => {
    try {
        res.body = UserService.createUser(req.body);
        next();
    } catch (err) {
        next(
            {
                message: err.message,
                status: 500,
            },
            req,
            res,
            next
        );
    }
};

const getAllUsers = (req, res, next) => {
    try {
        res.body = UserService.getAllUsers();
        next();
    } catch (err) {
        next(
            {
                message: err.message,
                status: 404,
            },
            req,
            res,
            next
        );
    }
};

const getByIdUser = (req, res, next) => {
    try {
        const id = req.params.id;
        res.body = UserService.search({ id });
        next();
    } catch (err) {
        next(
            {
                message: err.message,
                status: 404,
            },
            req,
            res,
            next
        );
    }
};

const removeUserById = (req, res, next) => {
    try {
        const id = req.params.id;
        res.body = UserService.removeUser(id);
        console.log(res.body);
        next();
    } catch (err) {
        next(
            {
                message: err.message,
                status: 404,
            },
            req,
            res,
            next
        );
    }
}

const updateUser = (req, res, next) => {
    try {
        const id = req.params.id;
        res.body = UserService.update(id, req.body);
        next();
    } catch (err) {
        next(
            {
                message: err.message,
                status: 500,
            },
            req,
            res,
            next
        );
    }
};

// TODO: Implement route controllers for user
router.post('/', [validateUser, createUser, responseMiddlewareCreateUser]);
router.put('/:id', [updateUser, responseMiddlewareUpdateUser]);
router.get('/', [getAllUsers, responseMiddlewareAllUsers]);
router.get('/:id', [getByIdUser, responseMiddlewareByIdUser]);
router.delete('/:id', [removeUserById, responseMiddlewareDeleteUser]);

module.exports = router;
