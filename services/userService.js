const { UserRepository } = require('../repositories/userRepository');

class UserService {
    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    createUser(user) {
        return UserRepository.create(user);
    }

    getAllUsers() {
        return UserRepository.getAll();
    }

    removeUser(id) {
        const itemsRemoved = UserRepository.delete(id);
        return itemsRemoved.length ? item : null;
    }

    update(id, data) {
        return UserRepository.update(id, data);
    }
}

module.exports = new UserService();
